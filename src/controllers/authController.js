const express = require('express');
const router = new express.Router();

const {registration, loginUser} = require('../services/authService');

router.post('/register', async (req, res) => {
  try {
    const {
      username,
      password,
    } = req.body;
    await registration({username, password});

    res.json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.post('/login', async (req, res) => {
  try {
    const {
      username,
      password,
    } = req.body;
    const token = await loginUser({username, password});

    res.json({message: 'Success', jwt_token: token});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

module.exports = {
  authController: router,
};
