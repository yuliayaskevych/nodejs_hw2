const express = require('express');
const router = new express.Router();
const {
  getNotes,
  addNote,
  getNoteById,
  updateNote,
  changeCompleted,
  deleteNote,
} = require('../services/notesService');

router.get('/', async (req, res) => {
  try {
    const {userId} = req.user;
    const {offset,
      limit,
      count,
      notes} = await getNotes(userId);
    res.json({offset, limit, count, notes});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.post('/', async (req, res) => {
  try {
    const {userId} = req.user;
    await addNote(userId, req.body);

    res.json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.get('/:id', async (req, res) => {
  try {
    const {userId} = req.user;
    const note = await getNoteById(userId, req.params.id);

    res.status(200).json({note});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.put('/:id', async (req, res) => {
  try {
    const {userId} = req.user;

    await updateNote(userId, req.params.id, req.body);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.patch('/:id', async (req, res) => {
  try {
    const {userId} = req.user;

    await changeCompleted(userId, req.params.id);
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const {userId} = req.user;

    await deleteNote(userId, req.params.id);

    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(400).json({message: err.message});
  }
});

module.exports = {
  notesController: router,
};
