const {Note} = require('../models/noteModel');

const getNotes = async (userId, offset = 0, limit = 3) => {
  const count = await Note.find({userId}).countDocuments();
  const notes = await Note.find({userId}, '-__v')
      .skip(+offset)
      .limit(+limit);

  if (!notes) {
    throw new Error('No notes whit such id found');
  }

  return {
    offset,
    limit,
    count,
    notes,
  };
};

const addNote = async (userId, notePayload) => {
  const note = new Note({...notePayload, userId});
  await note.save();
};

const getNoteById = async (userId, _id) => {
  const note = await Note.findOne({_id, userId});
  if (!note) {
    throw new Error('No note found');
  }
  return note;
};

const updateNote = async (userId, _id, {text}) => {
  const note = await Note.findOne({_id, userId});
  console.log(text);
  if (!note) {
    throw new Error('No note found');
  }
  await Note.updateOne({_id, userId}, {
    $set: {
      text,
    },
  });
  return note;
};

const changeCompleted = async (userId, _id) => {
  const note = await Note.findOne({_id, userId});
  if (!note) {
    throw new Error('No note found');
  }
  await Note.updateOne({_id}, {
    $set: {
      completed: !note.completed,
    },
  });
  return note;
};

const deleteNote = async (userId, _id) => {
  const note = await Note.findOneAndDelete({_id, userId});
  if (!note) {
    throw new Error('No note found');
  }
  return note;
};

module.exports = {
  getNotes,
  addNote,
  getNoteById,
  updateNote,
  changeCompleted,
  deleteNote,
};
