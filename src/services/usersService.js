const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUser = async (_id) => {
  const user = await User.findOne({_id});
  console.log('service: ', user);
  console.log('service ID: ', _id);

  return user;
};

const deleteUser = async (_id) => {
  const user = await User.findOneAndDelete({_id});
  if (!user) {
    throw new Error('No user found');
  }
  return user;
};

const changePassword = async (_id, oldPassword, newPassword) => {
  const user = await User.findOne({_id});

  if (!user) {
    throw new Error('No user found');
  }
  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('Wrong password!');
  }
  await User.updateOne({_id}, {
    $set: {
      password: await bcrypt.hash(newPassword, 10),
    },
  });
};


module.exports = {
  getUser,
  deleteUser,
  changePassword,
};
