const express = require('express');
// const path = require('path');
const morgan = require('morgan');
const app = express();
const mongoose = require('mongoose');

const {notesController} = require('./controllers/notesController');
const {usersController} = require('./controllers/usersController');
const {authController} = require('./controllers/authController');
const {authMiddleware} = require('./middlewares/authMiddleware');

// app.use(express.static('./dist'));
app.use(express.json());
app.use(morgan('tiny'));


app.use('/api/auth', authController);
// private
app.use(authMiddleware);
app.use('/api/users/me', usersController);
app.use('/api/notes', notesController);

// myFirstDatabase nodejs_hw2 testjs_hw2
const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://yuliayaskevych:jlju@cluster0.ccskx.mongodb.net/testjs_hw2?retryWrites=true&w=majority',
        {useNewUrlParser: true, useUnifiedTopology: true});
    // , createIndexes: true
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
